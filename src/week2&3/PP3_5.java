import java.util.Scanner;
import java.text.DecimalFormat;

public class PP3_5{

   public static void main(String[] args)
   {
	double radius, surface, volume;

	Scanner scan = new Scanner(System.in);

	System.out.print("请输入球的半径： ");
	radius = scan.nextDouble();

	surface = 4 * Math.PI * Math.pow(radius, 2);
	volume = (4/3) * Math.PI * Math.pow(radius, 3);

	DecimalFormat fmt = new DecimalFormat("0.####");

	System.out.println("表面积 = " + fmt.format(surface));
	System.out.println("体积 = " + fmt.format(volume));
   }
}
