import java.util.Scanner;
public class PP2_10
{
   public static void main(String[] args)
   {
	int sideLength, perimeter, area;
	Scanner scan=new Scanner(System.in);
	
	System.out.print("请输入正方形的边长: ");
	sideLength=scan.nextInt();

	perimeter=4*sideLength; 
	area=sideLength*sideLength;

	System.out.println("周长: "+perimeter+"\n面积: "+area);
   }
}
