import java.util.Scanner;
public class PP2_4
{
   public static void main(String[] args)
   {
	float fahrenheitTemp, celsiusTemp;
	final float FACTOR=9/5;

	Scanner scan=new Scanner(System.in);

	System.out.print("输入华氏温度：");
	fahrenheitTemp=scan.nextFloat();

	celsiusTemp=(fahrenheitTemp-32)/FACTOR;
	System.out.println("摄氏度: "+celsiusTemp);
   }
}
