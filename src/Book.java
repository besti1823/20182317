public class Book
{
   private String bookname;
   private String author;
   private String press;
   public String copyrightdate;

   public Book(String name,String writer,String publisher,String copyRightdate)
   {
        bookname = name;
        author = writer;
        press = publisher;
        copyrightdate = copyRightdate;
   }

   public String getName()
   {
        return bookname;
   }

   public String getAuthor()
   {
        return author;
   }

   public String getPress()
   {
        return press;
   }

   public String getDate()
   {
        return copyrightdate;
   }

   public void setName(String nameAlt)
   {
	bookname = nameAlt;
   }

   public void setAuthor(String authorAlt)
   {
	author = authorAlt;
   }

   public void setPress(String pressAlt)
   {
	press = pressAlt;
   }

   public void setDate(String copyrightdateAlt)
   {
	copyrightdate = copyrightdateAlt;
   }
   public String toString()
   {
        return "书名: " + bookname + "\n" + "作者: " + author +
                "\n" + "出版社: " + press + "\n" + "出版时间: " + copyrightdate;
   }
}
